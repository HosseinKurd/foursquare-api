package com.cafebazaar.sample.helper;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.cafebazaar.sample.R;

/**
 * Moves & Transportation between Activities
 */
public class ActivityHelper {

    private static ActivityHelper instance = null;

    private ActivityHelper() {
        // Private Constructor
    }

    public static ActivityHelper getInstance() {

        if (instance == null) {
            instance = new ActivityHelper();
        }
        return instance;
    }

    /**
     * Move From Current Activity to Another one
     *
     * @param from                Current Activity You are in
     * @param destinationActivity Destination Activity You want to move to
     * @param finishCurrent       Finihelpersh Current Activity? Current Activity will Finish if `True`
     */
    public void moveTo(@NonNull Activity from, Class<?> destinationActivity, boolean finishCurrent) {
        Intent intent = new Intent(from, destinationActivity);
        from.startActivity(intent);
        // from.overridePendingTransition(R.anim.enter, R.anim.exit);
        if (finishCurrent) {
            from.finish();
        }
    }
}