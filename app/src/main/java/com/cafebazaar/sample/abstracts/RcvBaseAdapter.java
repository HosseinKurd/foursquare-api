package com.cafebazaar.sample.abstracts;

import android.content.Context;
import android.view.LayoutInflater;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * Created by Kurdia.Epic on 5/12/2018.
 */

public abstract class RcvBaseAdapter<VH extends RecyclerView.ViewHolder, T> extends RecyclerView.Adapter<VH> {

    protected List<T> items;
    protected Context ctxAdapter;
    protected LayoutInflater inflater;
    protected OnItemClickListener<T> onItemClickListener;

    /**
     *
     * @param position Index of target item
     * @return T type
     */
    public T getItem(int position) {
        return (T) items.get(position);
    }

    /**
     * Set list of Items
     * @param items T typed item {@link List}
     */
    public void setItems(List<T> items) {
        if(items == null) {
            return;
        }
        this.items = items;
        /*if(this.items == null) {
            this.items = new ArrayList<>();
        }
        this.items.addAll(items);*/
    }

    /**
     * Default Constructor
     * @param context current {@link Context}
     * @param items   T typed item {@link List}
     */
    public RcvBaseAdapter(Context context, List<T> items) {
        inflater = LayoutInflater.from(context);
        this.ctxAdapter = context;
        this.items = items;
    }

    /**
     * Get T typed item {@link List}
     * @return T typed item {@link List}
     */
    public List<T> getItemList() {
        return items;
    }

    /**
     * Get size of Items
     * @return Item size
     */
    @Override
    public final int getItemCount() {
        return items.size();
    }

    /**
     * Set Item Click Listener callback
     * @param mListener {@link OnItemClickListener<T>}
     */
    public void setOnItemClickListener(OnItemClickListener<T> mListener) {
        this.onItemClickListener = mListener;
    }

    /**
     * Item Click Listener callback Interface
     * @param <T> Type of item Extends {@link Object}
     */
    public interface OnItemClickListener<T> {
        void onClicked(int actionId, int position, T item);
    }

}