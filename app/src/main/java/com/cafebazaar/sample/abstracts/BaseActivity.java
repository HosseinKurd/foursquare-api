package com.cafebazaar.sample.abstracts;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.cafebazaar.sample.BuildConfig;
import com.cafebazaar.sample.R;
import com.cafebazaar.sample.config.Utility;

import dagger.android.AndroidInjection;

public abstract class BaseActivity<T extends ViewDataBinding, V extends BaseViewModel> extends AppCompatActivity {

    protected final String TAG = getClass().getSimpleName() + "_TAG";

    private ProgressDialog progress;
    private T mViewDataBinding;
    private V mViewModel;

    protected Activity getActivity() {
        return this;
    }

    public abstract int getBindingVariable();

    public abstract @LayoutRes
    int getLayoutId();

    public abstract V getViewModel();

    public ProgressDialog getProgressBar() {
        return progress;
    }

    public BaseActivity<T, V> setProgressBar(ProgressDialog progress) {
        this.progress = progress;
        return this;
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void showLoading() {
        hideLoading();
        progress = Utility.getInstance().showLoadingDialog(this);
    }

    public void hideLoading() {
        if (progress != null && progress.isShowing()) {
            progress.cancel();
        }
    }

    public void performDependencyInjection() {
        AndroidInjection.inject(this);
    }

    private void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        this.mViewModel = mViewModel == null ? getViewModel() : mViewModel;
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.executePendingBindings();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        performDependencyInjection();
        super.onCreate(savedInstanceState);
        performDataBinding();
    }

    @Override
    protected void onStart() {
        if (BuildConfig.DEBUG) {
            Log.e("TAG_TAG", "Where am i ? " + getClass().getSimpleName());
        }
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.fadeout);
    }
}