package com.cafebazaar.sample.abstracts;

import androidx.databinding.ViewDataBinding;

public abstract class BaseFragmentActivity<T extends ViewDataBinding, V extends BaseViewModel>
        extends BaseActivity<T, V> {

}