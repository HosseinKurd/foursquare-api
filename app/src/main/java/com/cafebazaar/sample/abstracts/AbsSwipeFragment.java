package com.cafebazaar.sample.abstracts;

import android.app.ProgressDialog;

import com.hosseinkurd.kurdiautils.views.fragments.AbsSwipeFrg;

/**
 * Swipeable Fragment, Swipe Vertically or Horizontally
 */
public abstract class AbsSwipeFragment extends AbsSwipeFrg {

    private ProgressDialog progress;

    /**
     * Show <string>Is loading ...</string> Dialog on screen
     */
    protected void showLoading() {
        // Log.w(TAG, getClass().getSimpleName() + " showLoading " + Thread.currentThread().getName())
        /*if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }*/
        hideLoading();
        // progress = Utility.getInstance().showLoadingDialog(getContext());
    }

    /**
     * Hide <string>Is loading ...</string> Dialog on screen
     */
    protected void hideLoading() {
        // Log.w(TAG, getClass().getSimpleName() + " hideLoading " + Thread.currentThread().getName());
        /*if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).hideLoading();
        }*/
        if (progress != null && progress.isShowing()) {
            progress.cancel();
        }
    }

}