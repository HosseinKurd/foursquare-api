package com.cafebazaar.sample.di.component;

import android.app.Application;

import com.cafebazaar.sample.config.App;
import com.cafebazaar.sample.di.builder.ActivityBuilder;
import com.cafebazaar.sample.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;


/**
 * The {@link AppComponent} class prepares Objects for injection
 */
@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, ActivityBuilder
        .class})
public interface AppComponent {
    void inject(App app);

    /**
     * Builder Component interface
     */
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}