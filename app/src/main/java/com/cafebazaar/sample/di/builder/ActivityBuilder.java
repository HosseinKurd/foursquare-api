package com.cafebazaar.sample.di.builder;

import com.cafebazaar.sample.ui.controller.ControllerActivity;
import com.cafebazaar.sample.ui.controller.ControllerActivityModule;
import com.cafebazaar.sample.ui.splash.SplashActivity;
import com.cafebazaar.sample.ui.splash.SplashActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * The {@link ActivityBuilder} class Builds ViewModels, Module And adapt to a Specific Activity
 */
@Module
public abstract class ActivityBuilder {

    /**
     * Definition of {@link SplashActivity} Module
     *
     * @return {@link SplashActivity}
     */
    @ContributesAndroidInjector(modules = SplashActivityModule.class)
    abstract SplashActivity bindSplashActivity();

    /**
     * Definition of {@link ControllerActivity} Module
     *
     * @return {@link ControllerActivity}
     */
    @ContributesAndroidInjector(modules = ControllerActivityModule.class)
    abstract ControllerActivity bindControllerActivity();
}