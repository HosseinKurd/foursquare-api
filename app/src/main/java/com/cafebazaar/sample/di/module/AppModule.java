package com.cafebazaar.sample.di.module;

import android.app.Application;
import android.content.Context;

import com.cafebazaar.sample.BuildConfig;
import com.cafebazaar.sample.R;
import com.cafebazaar.sample.config.AppDataManager;
import com.cafebazaar.sample.database.DbRepo;
import com.cafebazaar.sample.database.tables.MyObjectBox;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.objectbox.BoxStore;
import io.objectbox.android.AndroidObjectBrowser;

/**
 * The {@link AppModule} class provides objects to inject
 */
@Module
public class AppModule {

    /**
     * Provide {@link DbRepo}
     *
     * @param boxStore To return DbRepo
     * @return Singleton {@link DbRepo}
     */
    @Provides
    @Singleton
    DbRepo provideDbRepo(BoxStore boxStore) {
        return new DbRepo(boxStore);
    }

    /**
     * Provide {@link BoxStore}
     *
     * @param context Application {@link Context}
     * @return Singleton {@link BoxStore}
     */
    @Provides
    @Singleton
    BoxStore provideBoxStore(Context context) {
        BoxStore boxStore = MyObjectBox.builder()
                .androidContext(context)
                .name(context.getString(R.string.DB_VER))
                .build();
        if (BuildConfig.DEBUG) {
            new AndroidObjectBrowser(boxStore).start(context);
        }
        return boxStore;
    }

    /**
     * Provide {@link Context}
     *
     * @param application {@link Application}
     * @return Singleton {@link Application}'s {@link Context}
     */
    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    /**
     * Provide {@link AppDataManager}
     *
     * @param context Application {@link Context}
     * @return Singleton {@link AppDataManager}
     */
    @Provides
    @Singleton
    AppDataManager provideDataManager(Context context) {
        return new AppDataManager(context);
    }

}