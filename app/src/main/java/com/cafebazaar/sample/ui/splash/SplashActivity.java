package com.cafebazaar.sample.ui.splash;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;

import androidx.annotation.Nullable;

import com.cafebazaar.sample.config.AppDataManager;
import com.cafebazaar.sample.BR;
import com.cafebazaar.sample.R;
import com.cafebazaar.sample.abstracts.BaseActivity;
import com.cafebazaar.sample.databinding.ActivitySplashBinding;
import com.cafebazaar.sample.helper.ActivityHelper;
import com.cafebazaar.sample.ui.controller.ControllerActivity;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity<ActivitySplashBinding, SplashViewModel> implements SplashNavigator {

    @Inject
    AppDataManager appDataManager;

    @Inject
    SplashViewModel viewModel;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public SplashViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getViewModel().setNavigator(this);
        // new CacheHelper().clear(this);
        getViewModel().onCreate();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getViewModel().onStart();
    }

    @Override
    public AppDataManager getAppDataManager() {
        return appDataManager;
    }

    @Override
    public Activity getCurrentActivity() {
        return this;
    }

    @Override
    public void goToController() {
        new Handler().postDelayed(() -> {
            if (isFinishing()) {
                return;
            }
            ActivityHelper.getInstance().moveTo(this, ControllerActivity.class, true);
        }, 2000);
    }
}