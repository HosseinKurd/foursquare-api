package com.cafebazaar.sample.ui.splash;

import android.app.Activity;

import com.cafebazaar.sample.config.AppDataManager;

public interface SplashNavigator {

    AppDataManager getAppDataManager();

    Activity getCurrentActivity();

    void goToController();
}