package com.cafebazaar.sample.ui.controller;

import android.app.Activity;

import com.cafebazaar.sample.config.AppDataManager;
import com.cafebazaar.sample.databinding.ActivityControllerBinding;
import com.google.android.gms.maps.SupportMapFragment;


public interface ControllerNavigator {

    int REQUEST_CODE_ASK_PERMISSIONS = 101;

    AppDataManager getAppDataManager();

    ActivityControllerBinding getActivityBinding();

    Activity getCurrentActivity();

    SupportMapFragment getSupportMapFragment();

}