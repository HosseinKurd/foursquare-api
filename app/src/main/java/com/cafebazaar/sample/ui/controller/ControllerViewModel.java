package com.cafebazaar.sample.ui.controller;

import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.cafebazaar.sample.abstracts.BaseViewModel;
import com.cafebazaar.sample.database.tables.Place;
import com.cafebazaar.sample.services.GPSTrackerService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;

public class ControllerViewModel extends BaseViewModel<ControllerNavigator> implements OnMapReadyCallback {

    private ArrayList<Place> items = new ArrayList<>();

    void onCreate() {
        getNavigator().getSupportMapFragment().getMapAsync(this);
    }

    void onStart() {
        investigatePermission();

    }

    private void setAdapter() {
        PlaceAdapter adapter = new PlaceAdapter(getNavigator().getCurrentActivity(), items);
        adapter.setOnItemClickListener((actionId, position, item) -> {
            // Open Detail Fragment
        });
        getNavigator().getActivityBinding().recyclerView.setAdapter(adapter);
    }

    private void trackUser() {
        GPSTrackerService gpsTrackerService = new GPSTrackerService(getNavigator().getCurrentActivity());
        if (gpsTrackerService.canGetLocation()) {
            Log.w("TAG_TAG", "Location LAT LNG: " + gpsTrackerService.getLatitude() + " , " + gpsTrackerService.getLongitude());
        } else {
            gpsTrackerService.showSettingsAlert();
        }
    }

    void onStop() {
    }

    void onResume() {

    }

    /**
     * Invest Necessary Permission
     */
    private void investigatePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getNavigator().getCurrentActivity(),
                    android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(getNavigator().getCurrentActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getNavigator().getCurrentActivity(),
                        new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                                android.Manifest.permission.ACCESS_FINE_LOCATION}, getNavigator().REQUEST_CODE_ASK_PERMISSIONS);
            } else {
                onPermissionGranted();
            }
        } else {
            Log.e(TAG, "CheckACCESS_COARSE_LOCATION & ACCESS_FINE_LOCATION Permissions: No need to check permissions. SDK version <= LOLLIPOP.");
        }
    }

    /**
     * Callback for the result from requesting permissions
     *
     * @param requestCode  The request code passed in {@link android.app.Activity#requestPermissions(String[], int)}
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     */
    void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: " + requestCode + " " + Arrays.toString(permissions) + " " + Arrays.toString(grantResults));
        if (requestCode == getNavigator().REQUEST_CODE_ASK_PERMISSIONS) {
            for (int i = 0; i < permissions.length; i++) {
                switch (permissions[i]) {
                    case android.Manifest.permission.WRITE_EXTERNAL_STORAGE:
                        if (grantResults.length > i && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.d(TAG, "onRequestPermissionsResult --> WRITE_EXTERNAL_STORAGE PERMISSION_GRANTED");
                        }
                        break;
                    case android.Manifest.permission.ACCESS_COARSE_LOCATION:
                        if (grantResults.length > i && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.d(TAG, "onRequestPermissionsResult --> ACCESS_COARSE_LOCATION PERMISSION_GRANTED");
                        }
                }
            }
            onPermissionGranted();
        }
    }

    /**
     * Grant Necessary Permission
     */
    private void onPermissionGranted() {
        if ((ContextCompat.checkSelfPermission(
                getNavigator().getCurrentActivity(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                & ContextCompat.checkSelfPermission(
                getNavigator().getCurrentActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)) == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "onPermissionGranted, Tracking User.");
            trackUser();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        googleMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }
}