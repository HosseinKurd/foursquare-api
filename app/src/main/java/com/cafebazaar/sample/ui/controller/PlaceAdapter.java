package com.cafebazaar.sample.ui.controller;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cafebazaar.sample.R;
import com.cafebazaar.sample.abstracts.RcvBaseAdapter;
import com.cafebazaar.sample.customs.BasicTextView;
import com.cafebazaar.sample.database.tables.Place;

import java.util.List;

public class PlaceAdapter extends RcvBaseAdapter<PlaceAdapter.GroupViewHolder, Place> {

    PlaceAdapter(Context context, List<Place> items) {
        super(context, items);
    }

    @NonNull
    @Override
    public GroupViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new GroupViewHolder(
                inflater.inflate(R.layout.adapter_place, viewGroup, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull GroupViewHolder groupViewHolder, int i) {
        groupViewHolder.onFill(groupViewHolder.getAdapterPosition());
    }

    class GroupViewHolder extends RecyclerView.ViewHolder {
        private BasicTextView txtGroup, txtRate;
        private ImageView imgThumb;
        private View view;

        GroupViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            txtGroup = itemView.findViewById(R.id.txt_title);
            txtRate = itemView.findViewById(R.id.txt_rate);
            imgThumb = itemView.findViewById(R.id.img_thumb);
        }

        private void onFill(int position) {
            Place item = items.get(position);
            txtGroup.setText(item.getTitle());
            txtRate.setText(item.getRate());
            view.setOnClickListener(v -> {
                if (onItemClickListener != null) {
                    onItemClickListener.onClicked(v.getId(), position, item);
                }
            });
        }

    }
}