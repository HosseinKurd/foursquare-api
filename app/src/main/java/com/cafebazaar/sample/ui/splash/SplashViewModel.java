package com.cafebazaar.sample.ui.splash;

import com.hosseinkurd.kurdiautils.toolbox.helpers.UIH;
import com.cafebazaar.sample.abstracts.BaseViewModel;

public class SplashViewModel extends BaseViewModel<SplashNavigator> {

    void onCreate() {
        getNavigator().goToController();
    }

    void onStart() {
        UIH.getScreenDetail(getNavigator().getCurrentActivity());
        UIH.createDirectories(getNavigator().getCurrentActivity());

    }
}