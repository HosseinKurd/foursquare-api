package com.cafebazaar.sample.ui.controller;

import dagger.Module;
import dagger.Provides;

@Module
public class ControllerActivityModule {

    @Provides
    ControllerViewModel provideControllerViewModel() {
        return new ControllerViewModel();
    }
}