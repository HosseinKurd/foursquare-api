package com.cafebazaar.sample.ui.controller;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.cafebazaar.sample.BR;
import com.cafebazaar.sample.R;
import com.cafebazaar.sample.abstracts.BaseActivity;
import com.cafebazaar.sample.config.AppDataManager;
import com.cafebazaar.sample.databinding.ActivityControllerBinding;
import com.google.android.gms.maps.SupportMapFragment;

import javax.inject.Inject;

/**
 * The {@link ControllerActivity} is Responsive to Lets User to Visit other Pages.
 */
public class ControllerActivity extends BaseActivity<ActivityControllerBinding, ControllerViewModel> implements ControllerNavigator {

    @Inject
    AppDataManager appDataManager;

    @Inject
    ControllerViewModel viewModel;
    private long lastPress = 0;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_controller;
    }

    @Override
    public ControllerViewModel getViewModel() {
        return viewModel;
    }

    @Override
    public void onBackPressed() {
        if (lastPress + 2000 < System.currentTimeMillis()) {
            lastPress = System.currentTimeMillis();
            Toast.makeText(this, R.string.msg_press_again_to_exit, Toast.LENGTH_LONG).show();
            return;
        }
        super.onBackPressed();
        System.exit(0);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getViewModel().setNavigator(this);
        getViewModel().onCreate();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getViewModel().onStart();
    }

    @Override
    protected void onStop() {
        getViewModel().onStop();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getViewModel().onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getViewModel().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public AppDataManager getAppDataManager() {
        return appDataManager;
    }

    @Override
    public ActivityControllerBinding getActivityBinding() {
        return getViewDataBinding();
    }

    @Override
    public Activity getCurrentActivity() {
        return this;
    }

    @Override
    public SupportMapFragment getSupportMapFragment() {
        return (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    }
}