package com.cafebazaar.sample.customs;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.hosseinkurd.kurdiautils.toolbox.settings.Settings;
import com.cafebazaar.sample.R;

import javax.annotation.Nullable;

public class BasicEditText extends AppCompatEditText {

    public BasicEditText(Context context) {
        super(context);
        initialize(context, null);
    }

    public BasicEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    public BasicEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context, attrs);
    }

    private void initialize(Context context, @Nullable AttributeSet attrs) {
        setTypeface(Settings.getInstance().getTypeface(context, R.string.font_basic));
        if (attrs != null) {

        }
    }

    public String getTextString() {
        if (getText() == null) {
            return "";
        }
        return getText().toString();
    }
}