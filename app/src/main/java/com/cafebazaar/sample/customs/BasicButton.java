package com.cafebazaar.sample.customs;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

import com.hosseinkurd.kurdiautils.toolbox.settings.Settings;
import com.cafebazaar.sample.R;

import javax.annotation.Nullable;

public class BasicButton extends AppCompatButton {

    public BasicButton(Context context) {
        super(context);
        initialize(context, null);
    }

    public BasicButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    public BasicButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context, attrs);
    }

    private void initialize(Context context, @Nullable AttributeSet attrs) {
        setTypeface(Settings.getInstance().getTypeface(context, R.string.font_basic));
        if (attrs != null) {

        }
    }
}