package com.cafebazaar.sample.customs;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.hosseinkurd.kurdiautils.toolbox.settings.Settings;
import com.cafebazaar.sample.R;

import javax.annotation.Nullable;

public class BasicTextView extends AppCompatTextView {

    public BasicTextView(Context context) {
        super(context);
        initialize(context, null);
    }

    public BasicTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    public BasicTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context, attrs);
    }

    private void initialize(Context context, @Nullable AttributeSet attrs) {
        setTypeface(Settings.getInstance().getTypeface(context, R.string.font_basic));
        if (attrs != null) {

        }
    }
}