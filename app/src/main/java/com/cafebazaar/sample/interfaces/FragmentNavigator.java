package com.cafebazaar.sample.interfaces;

import androidx.fragment.app.FragmentManager;

/**
 * Parent Fragment Navigator
 */
public interface FragmentNavigator {
    // int REQUEST_CODE_FRAGMENT_VALUE = -1;
    FragmentManager getMSupportFragmentManager();
}