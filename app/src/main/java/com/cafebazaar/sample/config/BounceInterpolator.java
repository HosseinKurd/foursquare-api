package com.cafebazaar.sample.config;

/**
 * The {@link BounceInterpolator} runs <strong>Bounce<strong> animation to view <strong>Objects</strong>
 */
public class BounceInterpolator implements android.view.animation.Interpolator {
    private double mAmplitude = 1;
    private double mFrequency = 10;

    private BounceInterpolator() {
        // Empty Constructor not Allowed
    }

    BounceInterpolator(double amplitude, double frequency) {
        mAmplitude = amplitude;
        mFrequency = frequency;
    }

    public float getInterpolation(float time) {
        return (float) (-1 * Math.pow(Math.E, -time / mAmplitude) *
                Math.cos(mFrequency * time) + 1);
    }
}