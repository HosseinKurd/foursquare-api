package com.cafebazaar.sample.config;

/**
 * The {@link AppConst} contains constant <strong>values</strong>
 */
public class AppConst {
    public static final String ITEM_KEY = "KEY_ITEM";
    public static final String ITEMS_KEY = "KEY_ITEMS";
    public static final String BASE_URL = "http://dev.achareh.ir";
    public static final Long connectTimeOut = 10L; // Second
    public static final Long readTimeOut = 10L; // Second
    public static final Long writeTimeOut = 10L; // Second
}