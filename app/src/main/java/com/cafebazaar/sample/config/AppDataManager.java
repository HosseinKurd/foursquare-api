package com.cafebazaar.sample.config;

import android.content.Context;

import androidx.annotation.NonNull;

import com.cafebazaar.sample.R;
import com.hosseinkurd.kurdiautils.toolbox.helpers.CacheHelper;
import com.cafebazaar.sample.R;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * The {@link AppDataManager} is a <strong>Singleton</strong> defined class witch
 * <strong>Save<strong> and <strong>Retrieve</strong> cached Data
 */

@Singleton
public class AppDataManager {

    private final Context mContext;
    private CacheHelper cacheHelper;

    @Inject
    public AppDataManager(Context mContext) {
        this.mContext = mContext;
        this.cacheHelper = new CacheHelper();
    }
}