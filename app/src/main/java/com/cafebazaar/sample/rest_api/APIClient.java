package com.cafebazaar.sample.rest_api;

import android.service.autofill.UserData;

import com.cafebazaar.sample.BuildConfig;
import com.cafebazaar.sample.config.AppConst;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public class APIClient {

    public Retrofit getDefaultClient() {
        Interceptor interceptor = chain -> {
            Request original = chain.request();
            Request.Builder builder = original.newBuilder();
            // builder.addHeader("Accept", "application/json");
            builder.addHeader("Content-Type", "Application/json");
            builder.addHeader("Username", "HosseinKurd");
            builder.addHeader("Password", "12345678");
            builder.header("X-DEVELOPER", "KURDIA");
            Request request = builder.method(original.method(), original.body()).build();
            return chain.proceed(request);
        };
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();

        if (BuildConfig.DEBUG) {
            // set your desired log level
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(AppConst.connectTimeOut, TimeUnit.SECONDS)
                .readTimeout(AppConst.readTimeOut, TimeUnit.SECONDS)
                .writeTimeout(AppConst.writeTimeOut, TimeUnit.SECONDS)
                // .cache(new Cache(context.getCacheDir(), 10 * 1024 * 1024))
                .addNetworkInterceptor(httpLoggingInterceptor)
                .addInterceptor(interceptor)
                .build();
        return new Retrofit.Builder()
                .baseUrl(AppConst.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    interface APIInterface {
        @FormUrlEncoded
        @POST("index.php?r=player/mobile-register")
        Call<UserData> register(@FieldMap Map<String, String> fields);

    }
}