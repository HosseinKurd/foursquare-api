package com.cafebazaar.sample.database.tables;

import android.os.Parcel;
import android.os.Parcelable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

/**
 * The {@link Place} class contains Information About each Place
 * Defined as table in ObjectBox ORM
 */
@Entity
public class Place implements Parcelable {

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel source) {
            return new Place(source);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };
    @Id
    private long id;
    private long lat;
    private long lng;
    private long tms;
    private int rate;
    private String title;
    private String address;
    private String description;
    public Place() {
    }

    protected Place(Parcel in) {
        this.id = in.readLong();
        this.lat = in.readLong();
        this.lng = in.readLong();
        this.tms = in.readLong();
        this.rate = in.readInt();
        this.title = in.readString();
        this.address = in.readString();
        this.description = in.readString();
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLat() {
        return lat;
    }

    public void setLat(long lat) {
        this.lat = lat;
    }

    public long getLng() {
        return lng;
    }

    public void setLng(long lng) {
        this.lng = lng;
    }

    public long getTms() {
        return tms;
    }

    public void setTms(long tms) {
        this.tms = tms;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeLong(this.lat);
        dest.writeLong(this.lng);
        dest.writeLong(this.tms);
        dest.writeInt(this.rate);
        dest.writeString(this.title);
        dest.writeString(this.address);
        dest.writeString(this.description);
    }
}