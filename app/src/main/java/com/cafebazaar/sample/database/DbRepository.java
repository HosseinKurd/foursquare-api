package com.cafebazaar.sample.database;

import androidx.annotation.IntDef;

import com.cafebazaar.sample.database.tables.Place;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

/**
 * DbRepository = DataBase Repository Interface
 */
public interface DbRepository {
    int SORT_BY_DATE_ASC = 1,
            SORT_BY_DATE_DESC = 2,
            SORT_BY_GROUP_ASC = 3,
            SORT_BY_GROUP_DESC = 4,
            SORT_BY_RFID_ASC = 5,
            SORT_BY_RFID_DESC = 6,
            SORT_BY_VID_ASC = 7,
            SORT_BY_VID_DESC = 8;

    long insertCoordinate(final Place Place);

    void insertCoordinates(List<Place> places);

    boolean isCoordinateEmpty();

    List<Place> getAllCoordinates();

    void removeCoordinateByID(long id);

    void removeCoordinatesByID(long... ids);

    void removeAllCoordinates();

    @IntDef(value = {
            SORT_BY_DATE_ASC,
            SORT_BY_DATE_DESC,
            SORT_BY_GROUP_ASC,
            SORT_BY_GROUP_DESC,
            SORT_BY_RFID_ASC,
            SORT_BY_RFID_DESC,
            SORT_BY_VID_ASC,
            SORT_BY_VID_DESC,
    })
    @Retention(RetentionPolicy.SOURCE)
    @interface SortOrder {

    }
}