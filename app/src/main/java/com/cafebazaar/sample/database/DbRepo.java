package com.cafebazaar.sample.database;

import androidx.annotation.NonNull;

import com.cafebazaar.sample.database.tables.Place;

import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;

/**
 * DbRepo = AppDataBase Repository
 */
public class DbRepo implements DbRepository {

    // private final String TAG = "BOX_TAG";
    private Box<Place> CoordinateBox;

    public DbRepo(@NonNull BoxStore boxStore) {
        this.CoordinateBox = boxStore.boxFor(Place.class);
    }

    @Override
    public long insertCoordinate(Place Place) {
        return CoordinateBox.put(Place);
    }

    @Override
    public void insertCoordinates(List<Place> places) {
        CoordinateBox.put(places);
    }

    @Override
    public boolean isCoordinateEmpty() {
        return CoordinateBox.isEmpty();
    }

    @Override
    public List<Place> getAllCoordinates() {
        return CoordinateBox.getAll();
    }

    @Override
    public void removeCoordinateByID(long id) {
        CoordinateBox.remove(id);
    }

    @Override
    public void removeCoordinatesByID(long... ids) {
        CoordinateBox.remove(ids);
    }

    @Override
    public void removeAllCoordinates() {
        CoordinateBox.removeAll();
    }
}